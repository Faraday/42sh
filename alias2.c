/*
** alias2.c for alias in /home/ledara_f/test/42sh/42sh
** 
** Made by ledara_f
** Login   <ledara_f@epitech.net>
** 
** Started on  Tue May  6 15:30:20 2014 ledara_f
** Last update Sun May 25 12:24:10 2014 elkaim_r
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "get_next_line.h"
#include "sh.h"

int		*malloc_static(void)
{
  int		*exclued;

  if ((exclued = malloc(1 * sizeof(int))) == NULL)
    return (0);
  exclued[0] = -1;
  return (exclued);
}

char		*recup_str(char *str, char *argv)
{
  int		i;

  if ((str = malloc((my_strlen(argv) + 1) * sizeof(char))) == NULL)
    return (0);
  i = -1;
  while (argv[++i] != ' ' && argv[i] != 0)
    str[i] = argv[i];
  str[i] = 0;
  return (str);
}

void		skip(int *i, int *exclued)
{
  int		k;

  k = 0;
  while (exclued && exclued[k] != -1)
    {
      if (*i == exclued[k])
	{
	  *i = *i + 1;
	  break ;
	}
      k = k + 1;
    }
}

char		*alias_loop(char **tabalias, char *str, int **exclued, char *argv)
{
  int		i;
  int		j;
  char		*str3;

  i = -1;
  while (tabalias && tabalias[++i] != NULL)
    {
      j = 0;
      skip(&i, *exclued);
      if (tabalias[i] != NULL)
	{
	  while (tabalias[i][j] != 0 && tabalias[i][j] != '=')
	    j++;
	  if (my_strncmp(str, tabalias[i], j) == 0)
	    {
	      str3 = my_strcat(&tabalias[i][my_strlen(str) + 1],
			       &argv[my_strlen(str)]);
	      append_int(exclued, i);
	      return (str3);
	    }
	}
      else
	break ;
    }
  return (NULL);
}

char		*alias(char **tabalias, char *argv)
{
  static int	*exclued = NULL;
  char		*str;
  char		*str3;

  str = NULL;
  if (exclued == NULL)
    if ((exclued = malloc_static()) == NULL)
      return (argv);
  if ((str = recup_str(str, argv)) == NULL)
    {
      exclued = NULL;
      return (argv);
    }
  str3 = alias_loop(tabalias, str, &exclued, argv);
  if (str3 == NULL)
    {
      exclued = NULL;
      return (argv);
    }
  exclued = NULL;
  return (str3);
}
