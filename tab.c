/*
** tab.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Fri Mar 14 09:57:15 2014 elkaim_r
** Last update Thu May 22 10:31:33 2014 amstut_a
*/

#include "sh.h"

int	tab_len(char **table)
{
  int	i;

  i = 0;
  while (table[i])
    ++i;
  return (i);
}
