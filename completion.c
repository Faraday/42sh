/*
** completion.c for completion in /home/amstut_a/rendu/42sh/debug
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Sat Mar 22 18:17:57 2014 amstut_a
** Last update Thu May 22 13:22:57 2014 amstut_a
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "sh.h"

t_list		*fill_linked_list(t_list *res, char *rep)
{
  t_list	*tmp;
  t_list	*new;

  if ((new = malloc(sizeof(*new))) == NULL)
    return (res);
  new->rep = my_strcpy(rep);
  new->next = NULL;
  if (res == NULL)
    return (new);
  else
    {
      tmp = res;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new;
      return (res);
    }
}

int		find_file(char *folder, char *sub, int pos, char **line)
{
  int		count;
  DIR		*dir;
  struct dirent	*read;
  t_list	*res;

  count = 0;
  if (init_autocomplete(&res, &count) == -1)
    return (1);
  if ((dir = opendir(folder)) != NULL)
    {
      while ((read = readdir(dir)) != NULL)
	{
	  if (my_strncmp(read->d_name, sub, my_strlen(sub)) == 0)
	    {
	      count++;
	      res = fill_linked_list(res, read->d_name);
	    }
	}
      closedir(dir);
    }
  if (count == 1)
    return (my_rewrite2(line, res->rep, pos, my_strlen(sub)));
  else if (count > 0)
    return (2);
  return (1);
}

int		find_begin(char *line, int pos)
{
  while (pos > 0 && line[pos - 1] != ' ' && line[pos - 1] != '\t')
    pos--;
  return (pos);
}

char		*get_sub(char *line, int pos)
{
  char		*sub;
  int		begin;
  int		i;

  i = 0;
  if ((sub = malloc(sizeof(char) * (my_strlen(line) + 1))) == NULL)
    return (NULL);
  begin = find_begin(line, pos);
  while (line[begin] != 0 && begin < pos)
    {
      sub[i] = line[begin];
      i++;
      begin++;
    }
  sub[i] = 0;
  return (sub);
}

void		my_autocomplete(char **line, int pos, t_flag *flags)
{
  char		*sub;
  char		*folder;

  sub = get_sub(*line, pos);
  folder = get_folder(*line, pos);
  sub = remove_folder_sub(sub, folder);
  if (my_strcmp(folder, "/") == 0)
    folder = "///";
  if (sub != NULL)
    {
      if (find_file(folder, sub, pos, line) == 0)
	flags->autocomplete = 1;
      else if (find_file(folder, sub, pos, line) == 2)
	{
	  flags->autocomplete = 2;
	  if (my_ask() == 1)
	    show_completion(folder, sub);
	}
    }
}
