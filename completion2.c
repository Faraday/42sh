/*
** completion2.c for completion2 in /home/amstut_a/rendu/42sh/debug
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Tue May  6 13:52:20 2014 amstut_a
** Last update Sun May 25 11:50:08 2014 elkaim_r
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <termios.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "sh.h"

int	init_autocomplete(t_list **res, int *count)
{
  if ((*res = xmalloc(sizeof(*res))) == NULL)
    return (-1);
  *res = NULL;
  *count = 0;
  return (0);
}

char	*get_sub_folder(char *sub)
{
  char	*folder;
  int	len;
  int	i;

  i = 0;
  len = my_strlen(sub);
  if ((folder = xmalloc(sizeof(char) * (len + 1))) == NULL)
    return (NULL);
  while (len >= 0 && sub[len] != '/')
    --len;
  --len;
  if (len <= 0 && sub[0] == '/')
    return ("/");
  if (len <= 0)
    return (NULL);
  while (i <= len)
    {
      folder[i] = sub[i];
      ++i;
    }
  folder[i] = 0;
  return (folder);
}

char	*get_folder(char *line, int pos)
{
  char	*sub;
  char	*folder;

  sub = get_sub(line, pos);
  if (sub != NULL)
    {
      if (sub[0] == '.' || sub[0] == '~' || sub[0] == '/')
	{
	  folder = get_sub_folder(sub);
	  if (folder == NULL)
	    folder = ".";
	}
      else
	{
	  folder = ".";
	}
      return (folder);
    }
  return (".");
}

char	*remove_folder_sub(char *sub, char *folder)
{
  char	*file;
  int	i;
  int	j;

  i = 0;
  j = 0;
  if ((file = xmalloc(sizeof(char) * (my_strlen(sub) + 1))) == NULL)
    return (NULL);
  while (folder[i])
    ++i;
  if (my_strlen(folder) == 1 && folder[0] == '.')
    i = 0;
  else if (my_strcmp(folder, "/") != 0)
    ++i;
  while (sub[i])
    {
      file[j] = sub[i];
      ++i;
      ++j;
    }
  file[j] = 0;
  return (file);
}
