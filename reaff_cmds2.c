/*
** reaff_cmds2.c for reaff_cmd2 in /home/amstut_a/42sh/autocomplete
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Thu May 15 15:22:01 2014 amstut_a
** Last update Fri May 23 12:27:00 2014 ledara_f
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <termios.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "sh.h"

void	reaff_cmd(char *line, int *pos, t_flag *flags, int width)
{
  char	*word;
  int	i;

  i = 0;
  my_putchar('\r');
  while (i < width)
    {
      my_putchar(' ');
      i++;
    }
  my_putchar('\r');
  my_putstr(line);
  if (flags->autocomplete == 0)
    my_replace_cursor(*pos + 1, line, width, flags);
  else
    {
      *pos = find_begin(line, *pos);
      word = get_next_word(line, *pos);
      *pos += (my_strlen(word) + 1);
      my_replace_cursor(*pos + 1, line, width, flags);
    }
}

void	reaff_autocomplete(char *line, int pos, t_flag *flags, int width)
{
  int	pos_end;

  pos_end = my_strlen(line);
  my_putstr(line);
  while (pos_end > pos + 1)
    {
      if (pos_end == width && flags->cur_lines > 0)
	{
	  get_high(width);
	  flags->cur_lines = 0;
	}
      go_left();
      pos_end--;
    }
}

void	reaff_multilines_down(char *line, int *pos, t_flag *flags, int width)
{
  int	i;
  char	*word;

  i = 0;
  my_putchar('\r');
  while (i < width)
    {
      my_putchar(' ');
      i++;
    }
  if (flags->cur_lines != 0)
    get_high(width);
  my_putchar('\r');
  my_putstr(line);
  if (flags->autocomplete == 0)
    my_replace_cursor(*pos + 1, line, width, flags);
  else
    {
      *pos = find_begin(line, *pos);
      word = get_next_word(line, *pos);
      *pos += (my_strlen(word) + 1);
      my_replace_cursor(*pos, line, width, flags);
    }
}

void	reaff_multilines_up(char *line, int *pos, t_flag *flags, int width)
{
  int	i;

  i = 0;
  my_putchar('\r');
  while (i < width)
    {
      my_putchar(' ');
      i++;
    }
  get_down();
  my_putchar('\r');
  i = 0;
  while (i < width)
    {
      my_putchar(' ');
      i++;
    }
  get_high_left(width);
  my_putstr(line);
  if (flags->cur_lines == 0)
    flags->cur_lines = 1;
  my_replace_cursor(*pos + 1, line, width, flags);
}
