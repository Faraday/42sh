/*
** ihnib.c for 42sh in /home/elkaim_r/Documents/42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Tue May  6 15:37:14 2014 elkaim_r
** Last update Sun May 25 10:30:34 2014 elkaim_r
*/

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sh.h"
#include "get_next_line.h"

int	ihnibited(char *str, int i)
{
  if (!i)
    return (0);
  if (str[i - 1] == '\\' || str[i - 1] == -1 * '\\')
    return (1);
  return (0);
}

void	turn_back(t_cmd *cmd)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  if (!cmd)
    return ;
  while (cmd)
    {
      if (!cmd->command)
	return ;
      while (cmd->command[i])
	{
	  while (cmd->command[i][j])
	    {
	      if (cmd->command[i][j] < 0)
		cmd->command[i][j] = cmd->command[i][j] * -1;
	      ++j;
	    }
	  ++i;
	  j = 0;
	}
      i = 0;
      cmd = cmd->next;
    }
}

int	mismatched(char *str, int i, char end)
{
  while (str[i] && (str[i] != end || ihnibited(str, i)))
    ++i;
  if (str[i])
    return (0);
  return (1);
}

void	double_quote(t_sh *sh, char quote)
{
  int	i;

  i = 0;
  if (!sh->input)
    return ;
  while (sh->input[i])
    {
      if (sh->input[i] == quote && !mismatched(sh->input, i + 1, quote)
	  && !ihnibited(sh->input, i))
	{
	  sh->input[i] = ' ';
	  i += 1;
	  while (sh->input[i] && (sh->input[i] != quote ||
				  ihnibited(sh->input , i)))
	    {
	      sh->input[i] = -1 * sh->input[i];
	      ++i;
	    }
	  sh->input[i] = ' ';
	}
      ++i;
    }
}
