/*
** main.c for test_history in /home/douzie_l/rendu/tmp/42sh/history
** 
*** Made by douzie_l
** Login   <douzie_l@epitech.net>
** 
** Started on  Tue May 13 13:59:35 2014 douzie_l
** Last update Sun May 25 18:46:05 2014 elkaim_r
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "sh.h"
#include "get_next_line.h"

int	to_history(char *str)
{
  int	fd;
  char	*tmp;
  char	**hist;

  hist = glob_star("~/");
  tmp = my_strcat(hist[0], ".42_history");
  if ((fd = open(tmp, O_CREAT | O_APPEND | O_RDWR,
		 S_IRUSR | S_IWUSR)) == -1)
    {
      return (fd);
    }
  tmp = my_strcat(str, "\n");
  write(fd, tmp, strlen(tmp));
  return (0);
}

int     aff_history(void)
{
  int   fd;
  int	rez;
  char  s[4096];
  char  *tmp;
  char  **hist;

  hist = glob_star("~/");
  tmp = my_strcat(hist[0], ".42_history");
  if (!tmp)
    return (0);
  if ((fd = open(tmp, O_RDONLY)) == -1)
    {
      printf("Can't open : %s\n", tmp);
      return (fd);
    }
  while ((rez = read(fd, s, 4096)) != -1)
    {
      if (rez == 0)
	return (0);
      s[rez] = 0;
      my_putstr(s);
    }
  return (0);
}
