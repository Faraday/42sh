/*
** bg.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Wed Apr 30 14:08:47 2014 elkaim_r
** Last update Fri May 23 12:17:10 2014 ledara_f
*/

#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include "sh.h"

void	check_process(int signum)
{
  int	i;
  int	status;
  pid_t	res;

  signum++;
  i = 0;
  status = 0;
  while (g_pid_tab[i] != -1)
    {
      res = waitpid(g_pid_tab[i], &status, WUNTRACED | WNOHANG);
      if (res && !WIFSTOPPED(status))
	{
	  printf("\n[%s]\tdone\n", g_process_tab[i]);
	  g_pid_tab = remove_int(g_pid_tab, i);
	  g_process_nb = remove_int(g_process_nb, i);
	  g_process_tab = remove_str(g_process_tab, i);
	  --i;
	}
      ++i;
    }
}
