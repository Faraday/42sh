/*
** my_setenv.c for setenv in /home/ledara_f/rendu/PSU_2013_minishell1
** 
** Made by ledara_f
** Login   <ledara_f@epitech.net>
** 
** Started on  Fri Dec 13 14:44:02 2013 ledara_f
** Last update Sun May 25 12:41:18 2014 elkaim_r
*/

#include <stdlib.h>
#include "sh.h"

char	**my_malloc_envp(char **envp, char **my_envp)
{
  int	i;

  i = 0;
  if (!envp)
    return (NULL);
  while (envp[i] != NULL)
    i = i + 1;
  my_envp = xmalloc((i + 2) * sizeof(char **));
  if (my_envp == NULL)
    return (NULL);
  i = 0;
  while (envp[i] != NULL)
    {
      my_envp[i] = envp[i];
      i = i + 1;
    }
  my_envp[i] = xmalloc((4096) * sizeof(char *));
  if (my_envp[i] == NULL)
    return (NULL);
  my_envp[i + 1] = NULL;
  return (my_envp);
}

char	*build_str(char **env)
{
  char	*str;
  int	i;

  i = 0;
  if ((str = calloc(4096, sizeof(char))) == NULL)
    return (NULL);
  while (env[i] != NULL)
    {
      str = my_strcat(str, env[i]);
      if (i + 1 != 2)
	str = my_strcat(str, "=");
      i++;
      if (i == 2)
	break ;
    }
  return (str);
}

char	**my_setenv(char **envp, char **env)
{
  char	**my_envp;
  char	*str;
  int	i;
  int	j;

  i = 0;
  j = 0;
  my_envp = NULL;
  if ((str = build_str(env)) == NULL)
    return (envp);
  if ((my_envp = my_malloc_envp(envp, my_envp)) == NULL)
    return (envp);
  while (my_envp[i] != NULL)
    i = i + 1;
  i = i - 1;
  while (str[j] != 0)
    {
      my_envp[i][j] = str[j];
      j = j + 1;
    }
  my_envp[i][j] = 0;
  my_envp[i + 1] = NULL;
  return (my_envp);
}
