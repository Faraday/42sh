/*
** my_unsetenv.c for unsetenv in /home/ledara_f/rendu/PSU_2013_minishell1
** 
** Made by ledara_f
** Login   <ledara_f@epitech.net>
** 
** Started on  Fri Dec 13 15:20:19 2013 ledara_f
** Last update Sun May 25 11:50:11 2014 elkaim_r
*/

#include <stdlib.h>
#include "sh.h"

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;

  i = 0;
  while (s1[i] != 0 && s2[i] != 0 && s1[i] == s2[i] && i < (n - 1))
    i = i + 1;
  return (s1[i] - s2[i]);
}

char	**my_malloc_unsetenv(char **envp, char **my_envp, int n)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (envp[i] != NULL)
    i = i + 1;
  my_envp = xmalloc((i + 1) * sizeof(char **));
  if (my_envp == NULL)
    return (NULL);
  i = 0;
  while (envp[i] != NULL)
    {
      if (i == n)
	i = i + 1;
      my_envp[j] = envp[i];
      if (envp[i] != NULL)
	i = i + 1;
      j = j + 1;
    }
  my_envp[j] = NULL;
  return (my_envp);
}

char	**my_unsetenv(char **envp, char *env)
{
  char	**my_envp;
  char	*str;
  int	i;

  i = -1;
  my_envp = NULL;
  if ((str = xmalloc((my_strlen(env) + 10) * sizeof(char))) == NULL)
    return (envp);
  if (!my_strcmp(env, "--all"))
    {
      my_envp = xmalloc(1 * sizeof(char *));
      if (my_envp)
	my_envp[0] = NULL;
      return (my_envp);
    }
  while (env[++i] != '=' && env[i] != 0)
    str[i] = env[i];
  str[i] = 0;
  i = -1;
  while (envp[++i] != NULL)
    if ((my_strncmp(envp[i], str, my_strlen(str)) == 0))
      break ;
  if ((my_envp = my_malloc_unsetenv(envp, my_envp, i)) == NULL)
    return (envp);
  return (my_envp);
}
