/*
** show_list.c for show_list in /home/amstut_a/rendu/42sh/debug
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Mon Mar 31 10:28:27 2014 amstut_a
** Last update Sun May 25 12:26:10 2014 elkaim_r
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <termios.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "sh.h"

int		my_ask(void)
{
  int		i;
  char		*str;

  if ((str = xmalloc(sizeof(char) * 10)) == NULL)
    return (0);
  my_putchar('\n');
  my_putstr("Do you want to display possibilities ? [y/n]\n");
  while ((i = read(0, str, 1)) != -1)
    {
      str[i] = 0;
      if (str[0] == 'y')
	return (1);
      else
	return (0);
    }
  return (0);
}

void		show_files(char *folder, char *sub)
{
  DIR		*dir;
  struct dirent	*read;

  if ((dir = opendir(folder)) != NULL)
    while ((read = readdir(dir)) != NULL)
      {
	if (my_strncmp(read->d_name, sub, my_strlen(sub)) == 0)
	  if (my_strncmp(read->d_name, ".", 1) != 0 &&
	      my_strncmp(read->d_name, "..", 2) != 0)
	    {
	      my_putstr(BLUE);
	      my_putstr(read->d_name);
	      my_putstr(END);
	      my_putchar('\n');
	    }
      }
}

void		show_completion(char *folder, char *sub)
{
  int		i;

  i = 2000;
  while (i > 0)
    {
      go_right();
      i--;
    }
  my_putchar('\n');
  show_files(folder, sub);
}
