/*
** removetab.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Mon Apr 28 15:22:10 2014 elkaim_r
** Last update Sun May 25 12:33:54 2014 elkaim_r
*/

#include <stdlib.h>
#include "sh.h"

int	int_len(int *table)
{
  int	i;

  i = 0;
  while (table[i] != -1)
    ++i;
  return (i);
}

int	*remove_int(int *table, int idx)
{
  int	*new;
  int	i;
  int	j;

  i = 0;
  j = 0;
  new = xmalloc((int_len(table)) * sizeof(int *));
  if (!new)
    return (NULL);
  while (table && table[i] != -1)
    {
      if (i != idx)
	{
	  new[j] = table[i];
	  ++j;
	}
      ++i;
    }
  new[j] = -1;
  return (new);
}

char	**remove_str(char **table, int idx)
{
  char	**new;
  int	i;
  int	j;

  i = 0;
  j = 0;
  new = xmalloc((tab_len(table)) * sizeof(char **));
  if (!new)
    return (NULL);
  while (table && table[i])
    {
      if (i != idx)
	{
	  new[j] = table[i];
	  ++j;
	}
      ++i;
    }
  new[j] = NULL;
  return (new);
}
