/*
** execloop.c for 42sh in /home/elkaim_r/Documents/42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Wed May 21 13:41:10 2014 elkaim_r
** Last update Sun May 25 16:59:12 2014 elkaim_r
*/

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sh.h"

void	parse_alias(t_sh *sh, int i)
{
  int	o;
  char	*str;

  o = 0;
  str = NULL;
  while ((my_strcmp(str, sh->instruct[i])))
    {
      if (o > 1000)
	break ;
      str = sh->instruct[i];
      if (sh->alias)
	sh->instruct[i] = alias42(sh->instruct[i], &sh->alias);
      if (sh->instruct[i] == NULL)
	break ;
      o++;
    }
}

int	exec_arg(t_sh *sh, int i)
{
  sh->command = redi_command(sh, i);
  get_cmd_pipes(sh);
  turn_back(sh->cmd);
  if (sh->cmd && sh->cmd->command[0]
      && !my_strncmp("exit", sh->cmd->command[0], 4))
    {
      if (sh->cmd->command[1] != NULL)
	sh->exit = my_getnbr(sh->cmd->command[1]);
      sh->stop = 1;
      return (1);
    }
  if (sh-> cmd && sh->cmd->command[0] != 0)
    if (execute(sh, i) == -1)
      {
	sh->stop = 1;
	return (1);
      }
  return (0);
}
