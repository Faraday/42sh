##
## Makefile for Makefile in /home/elkaim_r/rendu/PSU_2013_my_ls
## 
## Made by elkaim_r
## Login   <elkaim_r@epitech.net>
## 
## Started on  Fri Nov 22 16:19:52 2013 elkaim_r
## Last update Sun May 25 17:15:09 2014 elkaim_r
##

CC	= gcc

CFLAGS	= -Wall -Wextra -W

NAME	= 42sh

SRC	= main.c \
	  get_next_line.c \
	  exec.c \
	  prompter.c \
	  luke.c \
	  vader.c \
	  my_env.c \
	  lib.c \
	  lib2.c \
	  manage.c \
	  supertab.c \
	  my_builtin.c \
	  my_setenv.c \
	  my_unsetenv.c \
	  my_cd.c \
	  my_cd2.c \
	  my_getnbr.c \
	  my_strcmp.c \
	  tab.c \
	  disp.c \
	  alpha.c \
	  continue.c \
	  file.c \
	  append.c \
	  redi.c \
	  alias.c \
	  removetab.c \
	  bg.c \
	  list.c \
	  alias2.c \
	  var.c \
	  set.c \
	  ihnib.c \
	  check_builtin.c \
	  history.c \
	  glob.c \
	  error.c \
	  execloop.c \
	  line_edition.c \
	  remove_prompt.c \
	  check_line.c \
	  show_list.c \
	  completion2.c \
	  completion.c \
	  canon.c \
	  reaff_cmds2.c \
	  reaff_cmds.c \
	  editing.c \
	  editing2.c \
	  str.c \
	  pipe.c \
	  builtin2.c

OBJ	= $(SRC:.c=.o)

all:	$(NAME)

$(NAME): $(OBJ)
	 gcc -o $(NAME) $(OBJ) -lncurses

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re
