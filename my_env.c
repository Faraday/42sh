/*
** my_env.c for env$ in /home/amstut_a/rendu/42sh
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Thu Mar 13 13:30:01 2014 amstut_a
** Last update Sun May 25 11:45:50 2014 elkaim_r
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sh.h"

char	**get_env(char **env)
{
  char	**envx;
  int	i;

  i = 0;
  if (!env)
    return (NULL);
  while (env[i])
    i++;
  if ((envx = xmalloc(sizeof(char **) * (i + 1))) == NULL)
    return (NULL);
  i = 0;
  while (env[i])
    {
      if ((envx[i] = xmalloc(sizeof(char) * (strlen(env[i]) + 1))) == NULL)
	return (NULL);
      envx[i] = strcpy(envx[i], env[i]);
      i++;
    }
  envx[i] = NULL;
  return (envx);
}
