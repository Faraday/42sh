/*
** alpha.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Thu Apr  3 14:07:08 2014 elkaim_r
** Last update Wed May 21 17:50:36 2014 ledara_f
*/

#include "sh.h"

int	is_alpha(char c)
{
  if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
    return (1);
  return (0);
}
