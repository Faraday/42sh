/*
** canonique.c for canon in /home/amstut_a/rendu/42sh/debug
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Thu Apr 10 15:48:52 2014 amstut_a
** Last update Thu May 22 07:53:36 2014 amstut_a
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <termios.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "sh.h"

void	canon_off(struct termios *a)
{
  if ((tcgetattr(0, a)) == -1)
    return ;
  a->c_lflag &= ~ICANON;
  a->c_lflag &= ~ECHO;
  a->c_cc[VMIN] = 1;
  a->c_cc[VTIME] = 0;
  if (tcsetattr(0, TCSANOW, a) == -1)
    return ;
}

void	re_canon(struct termios *a)
{
  a->c_lflag ^= ~ICANON;
  a->c_lflag ^= ~ECHO;
  a->c_cc[VMIN] = 1;
  if (tcsetattr(0, TCSANOW, a) == -1)
    return ;
}

void	go_left(void)
{
  char	*str;

  str = tgetstr("le", NULL);
  if (str != NULL)
    my_putstr(str);
}

void	go_right(void)
{
  char	*str;

  str = tgetstr("nd", NULL);
  if (str != NULL)
    my_putstr(str);
}
