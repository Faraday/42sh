/*
** test.c for test in /home/douzie_l/rendu/tmp/glob
** 
** Made by douzie_l
** Login   <douzie_l@epitech.net>
** 
** Started on  Thu Mar 20 12:00:38 2014 douzie_l
** Last update Fri May 23 13:04:12 2014 ledara_f
*/

#include <glob.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sh.h"

char		**glob_star(char *cmd)
{
  glob_t	new;
  char		*res;

  if ((res = malloc(1 * sizeof(char))) == NULL)
    return (my_str_to_wordtab(cmd, 0));
  if ((glob(cmd, GLOB_NOCHECK | GLOB_TILDE_CHECK | GLOB_BRACE, NULL, &new))
      != 0)
    return (my_str_to_wordtab(cmd, ' '));
  return (new.gl_pathv);
}

void		globber(char ***final, char *to_glob)
{
  char		**globbed;
  int		i;

  i = 0;
  globbed = glob_star(to_glob);
  if (!globbed)
    return ;
  while (globbed[i])
  {
    append_tab(final, globbed[i]);
    ++i;
  }
}
