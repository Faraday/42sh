/*
** list.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Sat May  3 14:06:16 2014 elkaim_r
** Last update Wed May 21 18:02:51 2014 ledara_f
*/

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "sh.h"

int	size_list(t_cmd *cmd)
{
  int	i;

  i = 0;
  while (cmd)
    {
      cmd = cmd->next;
      ++i;
    }
  return (i);
}

void	add_to_list(t_cmd **list, char **command)
{
  t_cmd	*new;

  new = malloc(1 * sizeof(*new));
  if (!new)
    return ;
  new->next = *list;
  new->command = command;
  *list = new;
}
