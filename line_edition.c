/*
** line_edition.c for 42sh in /home/amstut_a/rendu/42sh
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Wed Mar 19 13:33:19 2014 amstut_a
** Last update Sun May 25 16:56:08 2014 elkaim_r
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <termios.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "sh.h"
#include "get_next_line.h"

void			init_flags(t_flag *flags)
{
  flags->suppr = 0;
  flags->left_key = 0;
  flags->right_key = 0;
  flags->autocomplete = 0;
  flags->mid_write = 0;
  flags->add_letter = 0;
}

void			treat_chain(char *s,
				    int *pos,
				    t_flag *flags,
				    char **line)
{
  if (my_strlen(s) > 1)
    my_move(s, pos, flags);
  else if (s[0] == 127)
    {
      *pos = *pos - 1;
      flags->suppr = 1;
      my_erase(pos, line);
    }
  else if (s[0] == '\t')
    my_autocomplete(line, *pos, flags);
  else if (*pos != my_strlen(*line) - 1)
    {
      my_rewrite(line, s, pos);
      flags->mid_write = 1;
      *pos = *pos + 1;
    }
  else
    {
      add_letter(s, line, pos);
      flags->add_letter = 1;
    }
}

void			mid_while(t_flag *flags,
				  int *pos,
				  char *line,
				  struct winsize **winsize)
{
  *winsize = check_line(flags, line);
  if (*winsize != NULL)
    {
      fill_flags_line(flags, line, (*winsize)->ws_col);
      reaff_line(flags, line, pos, (*winsize)->ws_col);
      update_total_lines(flags, line, (*winsize)->ws_col);
    }
  else
    {
      fill_flags_line(flags, line, 80);
      reaff_line(flags, line, pos, 80);
      update_total_lines(flags, line, 80);
    }
}

void			read_while(struct termios *a, char *prompt,
				   char *s, char **line)
{
  t_flag		flags;
  struct winsize	*winsize;
  int			i;
  int			pos;
  int			len;

  flags.prompt = prompt;
  len = init_edition(&flags, line, &pos, flags.prompt);
  while ((i = read(0, s, 10)) > 0)
    {
      s[i] = 0;
      init_flags(&flags);
      if (s[0] == 10)
	{
	  re_canon(a);
	  break ;
	}
      else if ((s[0] == 4 && pos == my_strlen(prompt)) || my_strlen(s) == 0)
	break ;
      else
	treat_chain(s, &pos, &flags, line);
      mid_while(&flags, &pos, *line, &winsize);
      if (pos <= len + 2)
	pos = len + 1;
    }
}

char			*line_editor(char *prompt)
{
  struct termios	a;
  char			*s;
  char			*line;

  if (init_edition2(&a, &s, &line) == -1)
    {
      my_putstr(prompt);
      re_canon(&a);
      return (get_next_line(0));
    }
  read_while(&a, prompt, s, &line);
  my_putchar('\n');
  if (s[0] == 4 || my_strlen(s) == 0)
    {
      re_canon(&a);
      return (NULL);
    }
  line = remove_prompt(line, prompt);
  return (line);
}
