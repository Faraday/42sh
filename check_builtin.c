/*
** check_builtin.c for 42sh in /home/elkaim_r/Documents/42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Wed May 14 16:02:39 2014 elkaim_r
** Last update Sun May 25 12:50:42 2014 elkaim_r
*/

#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include "sh.h"

int	my_builtin_check(char **av)
{
  if (!strcmp(av[0], "cd") || !strcmp(av[0], "setenv") || !strcmp(av[0], "env")
      || !strcmp(av[0], "fg") || !strcmp(av[0], "bg") || !strcmp(av[0], "jobs")
      || !strcmp(av[0], "unsetenv") || !strcmp(av[0], "set")
      || !strcmp(av[0], "alias") || !my_strcmp(av[0], "unset")
      || !my_strcmp(av[0], "history") || !my_strcmp(av[0], "unalias"))
    return (0);
  return (1);
}
