/*
** lib.c for lib in /home/elkaim_r/rendu/CPE-2013-BSQ
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Mon Dec  2 16:08:06 2013 elkaim_r
** Last update Sun May 25 10:59:38 2014 elkaim_r
*/

#include <unistd.h>
#include <stdlib.h>

void	my_memset(char *str, char c, int i)
{
  int	j;

  j = 0;
  while (j < i)
    {
      str[j] = c;
      j++;
    }
}

void	my_putchar(char c)
{
  write(1, &c, 1);
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  if (!str)
    return (0);
  while (str[i] != 0)
    i++;
  return (i);
}

void	my_putstr(char *str)
{
  if (!str)
    return ;
  write(1, str, my_strlen(str));
}

void	my_puterror(char *str)
{
  if (!str)
    return ;
  write(2, str, my_strlen(str));
}
