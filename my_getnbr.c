/*
** my_getnbr.c for my_getnbr in /home/ledara_f/rendu/CPE_2013_Pushswap
** 
** Made by ledara_f
** Login   <ledara_f@epitech.net>
** 
** Started on  Fri Dec 20 14:27:24 2013 ledara_f
** Last update Wed May 21 18:03:48 2014 ledara_f
*/

#include "sh.h"

int	my_check_str(char c)
{
  if (!(c >= '0' && c <= '9'))
    return (1);
  else
    return (0);
}

int	my_getnbr(char *str)
{
  int	i;
  int	signe;
  int	nb;

  i = 0;
  nb = 0;
  signe = 1;
  while (str[i] == '-')
    {
      signe = -1 * signe;
      i = i + 1;
    }
  while (str[i] != '\0')
    {
      if ((my_check_str(str[i])) == 1)
	return (nb * signe);
      nb = (nb * 10) + (str[i] - 48);
      i = i + 1;
    }
  return (nb * signe);
}
