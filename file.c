/*
** file.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Tue Apr 22 15:58:24 2014 elkaim_r
** Last update Thu May 22 11:47:37 2014 amstut_a
*/

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "sh.h"
#include "get_next_line.h"

void		trunc_file(t_sh *sh, char *str)
{
  if (sh->fd_in != -1)
    {
      close(sh->fd_in);
      sh->fd_in = -1;
    }
  sh->fd_in = open(str, O_WRONLY | O_CREAT | O_TRUNC, 00400 | 00200);
  if (sh->fd_in == -1)
    {
      my_puterror("error: file ");
      my_puterror(str);
      my_puterror(" could not be opened.\n");
    }
}

void		forward_file(t_sh *sh, char *str)
{
  if (sh->fd_in != -1)
    {
      close(sh->fd_in);
      sh->fd_in = -1;
    }
  sh->fd_in = open(str, O_WRONLY | O_CREAT | O_APPEND, 00400 | 00200);
  if (sh->fd_in == -1)
    {
      my_puterror("error: file ");
      my_puterror(str);
      my_puterror(" could not be opened.\n");
    }
}

void		get_file(t_sh *sh, char *str)
{
  if (sh->fd_out != -1)
    {
      close(sh->fd_out);
      sh->fd_out = -1;
    }
  sh->fd_out = open(str, O_RDONLY);
  if (sh->fd_out == -1)
    {
      my_putstr("error: file ");
      my_putstr(str);
      my_putstr(" could not be opened\n");
    }
}

void		get_input(t_sh *sh, char *str)
{
  int		pipefd[2];
  char		*res;
  int		i;

  i = 0;
  if (sh->fd_out != -1)
    {
      close(sh->fd_out);
      sh->fd_out = -1;
    }
  pipe(pipefd);
  res = get_next_line(0);
  while (res && my_strcmp(res, str) && i <= 10000)
    {
      i += my_strlen(res);
      write(pipefd[1], res, my_strlen(res));
      write(pipefd[1], "\n", 1);
      res = get_next_line(0);
    }
  close(pipefd[1]);
  sh->fd_out = pipefd[0];
}
