/*
** builtin2.c for 42sh in /home/elkaim_r/Documents/42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Sat May 24 20:30:55 2014 elkaim_r
** Last update Sun May 25 17:14:34 2014 elkaim_r
*/

#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "sh.h"

void	builtin_3(char **av, t_sh *sh, int i)
{
  if (strcmp(av[0], "history") == 0)
    aff_history();
  else if (strcmp(av[0], "unalias") == 0)
    while (av[i] != 0)
      {
	if ((sh->alias = my_unsetenv(sh->alias, av[i])) == NULL)
	  return ;
	i = i + 1;
      }
  else if (strcmp(av[0], "unset") == 0)
    while (av[i] != 0)
      {
	if ((sh->local = my_unsetenv(sh->local, av[i])) == NULL)
	  return ;
	i = i + 1;
      }
}

void	builtin_2(char **av, char ***envp, t_sh *sh, int i)
{
  if (strcmp(av[0], "jobs") == 0)
    my_jobs();
  else if (strcmp(av[0], "unsetenv") == 0)
    while (av[i] != 0)
      {
	if ((*envp = my_unsetenv(*envp, av[i])) == NULL)
	  return ;
	i = i + 1;
      }
  else if (strcmp(av[0], "set") == 0 && sh->local)
    {
      if (av[i] == 0)
	aff_tab(sh->local);
      else
	if ((sh->local = my_set(sh->local, &av[1])) == NULL)
	  return ;
    }
  else if (strcmp(av[0], "alias") == 0)
    {
      if (av[i] == 0)
	aff_tab(sh->alias);
      else
	return ;
    }
  builtin_3(av, sh, i);
}
