/*
** editing.c for editing in /home/amstut_a/rendu/42sh/debug
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Sat Mar 22 18:15:59 2014 amstut_a
** Last update Thu May 22 11:26:19 2014 amstut_a
*/

#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <termios.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "sh.h"

void		my_erase(int *pos, char **line)
{
  char		*end;

  go_left();
  my_putchar(' ');
  if (&(*line)[*pos + 1] != NULL)
    end = &(*line)[*pos + 1];
  if (*pos >= 0)
    (*line)[*pos] = 0;
  if (end != NULL && *pos > 0)
    (*line) = my_strcat((*line), end);
}

void		my_rewrite(char **line, char *s, int *pos)
{
  char		*end;

  if (&line[0][*pos] != NULL)
    end = &line[0][*pos];
  line[0] = cut_chain(line[0], *pos);
  line[0] = my_strcat(line[0], s);
  if (end != NULL)
    *line = my_strcat(*line, end);
}

char		*erase_char(char *end)
{
  if (end == NULL)
    return (NULL);
  else
    return (&end[1]);
}

int		my_rewrite2(char **line, char *s, int pos, int length)
{
  static int	i = 0;
  char		*end;

  if (&(*line)[pos] != NULL)
    {
      end = my_strcpy(&(*line)[pos - 1]);
      end = erase_char(end);
    }
  if (i != 0)
    *line = cut_chain(*line, (pos - (length + 1)));
  else
    *line = cut_chain(*line, (pos - (length + 1)));
  ++i;
  s = my_strcat(" ", s);
  *line = my_strcat(*line, s);
  *line = my_strcat(*line, " ");
  if (end != NULL)
    *line = my_strcat(*line, end);
  return (0);
}
