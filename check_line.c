/*
** check_line.c for check_line in /home/amstut_a/42sh/autocomplete
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Thu May 15 15:18:50 2014 amstut_a
** Last update Sat May 24 20:42:11 2014 elkaim_r
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <termios.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "sh.h"

struct winsize		*check_line(t_flag *flags, char *line)
{
  struct winsize	*a;
  static int		i = 0;
  int			length;
  float			width;
  float			res;

  if ((a = malloc(sizeof(*a))) == NULL)
    return (NULL);
  if (ioctl(STDOUT_FILENO, TIOCGWINSZ, a) == -1)
    return (NULL);
  length = my_strlen(line);
  width = (float)a->ws_col;
  res = length / width;
  if (flags->cur_lines == 0)
    i = 0;
  if (res > 1.0 && res < 2.0 && flags->suppr == 0)
    {
      flags->cur_lines = 1;
      if (i == 0)
	{
	  my_putchar('\n');
	  i++;
	}
    }
  return (a);
}

int			init_edition(t_flag *flags, char **line, int *pos,
				     char *prompt)
{
  int			len;

  flags->cur_lines = 0;
  flags->total_lines = 0;
  *line = my_strcpy(prompt);
  len = my_strlen(prompt);
  *pos = len;
  my_putstr(*line);
  return (len);
}

int			init_edition2(struct termios *a, char **s, char **line)
{
  canon_off(a);
  if (tgetent("xterm", NULL) == -1)
    return (-1);
  if ((*s = calloc(11, sizeof(char))) == NULL)
    return (-1);
  if ((*line = calloc((sizeof(char) * 1500), (sizeof(char) * 1500))) == NULL)
    return (-1);
  return (0);
}

void			fill_flags_line(t_flag *flags, char *line, int width)
{
  if (my_strlen(line) > width && flags->total_lines == 0)
    flags->total_lines = 1;
  if (my_strlen(line) < width)
    {
      flags->total_lines = 0;
      flags->cur_lines = 0;
    }
}

void			update_total_lines(t_flag *flags, char *line,
					   int width)
{
  if (flags->cur_lines > 0 && my_strlen(line) >= width)
    flags->total_lines = 1;
}
