/*
** append.c for asm in /home/elkaim_r/rendu/CPE_2014_corewar/Asm
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Thu Mar  6 12:35:56 2014 elkaim_r
** Last update Sat May 24 12:18:07 2014 elkaim_r
*/

#include <stdlib.h>
#include "get_next_line.h"
#include "sh.h"

int	size_tab(char **table)
{
  int	i;

  i = 1;
  while (table[i - 1] != NULL)
    ++i;
  return (i);
}

int	size_int(int *table)
{
  int	i;

  i = 1;
  while (table[i - 1] != -1)
    ++i;
  return (i);
}

void	aff_tab(char **table)
{
  int	i;

  i = 0;
  if (!table)
    return ;
  while (table[i])
    {
      my_putstr(table[i]);
      my_putchar('\n');
      ++i;
    }
}

void	append_tab(char ***table, char *string)
{
  int	i;

  i = size_tab(*table);
  *table = realloc(*table, (i + 1) * sizeof(*table));
  if (*table == NULL)
    {
      my_putstr("error: can't perform malloc");
      return ;
    }
  (*table)[i - 1] = string;
  (*table)[i] = NULL;
}

void	append_int(int **table, int new)
{
  int	i;
  int	*save;

  save = *table;
  i = size_int(*table);
  *table = realloc(*table, (i + 1) * sizeof(int));
  if (*table == NULL)
    {
      *table = save;
      return ;
    }
  (*table)[i - 1] = new;
  (*table)[i] = -1;
}
