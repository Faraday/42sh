/*
** luke.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Thu Mar 13 14:05:30 2014 elkaim_r
** Last update Thu May 22 11:49:04 2014 amstut_a
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include "sh.h"
#include "get_next_line.h"

void	reset_signals(void)
{
  signal(SIGINT, SIG_DFL);
  signal(SIGTSTP, SIG_DFL);
  signal(SIGTTOU, SIG_DFL);
}

void	close_fds(int pipefd[], int pipesave[], t_sh *sh)
{
  if (pipefd[1] != -1)
    close(pipefd[1]);
  if (pipefd[0] != -1)
    close(pipefd[0]);
  if (pipesave[1] != -1)
    close(pipesave[1]);
  if (pipesave[0] != -1)
    close(pipesave[0]);
  if (sh->fd_in != -1)
    close(sh->fd_in);
  if (sh->fd_out != -1)
    close(sh->fd_out);
}

void	echo_args(char **args)
{
  int	i;

  i = 1;
  while (args[i])
    {
      my_putstr(args[i]);
      if (args[i + 1])
	my_putchar(' ');
      ++i;
    }
  my_putchar('\n');
}

void	child_exec(t_sh *sh, int count, int pipefd[], int pipesave[])
{
  reset_signals();
  if (sh->fd_in != -1)
    dup2(sh->fd_in, 1);
  if (sh->fd_out != -1)
    dup2(sh->fd_out, 0);
  if (sh->cmd->next)
    dup2(pipefd[0], 0);
  if (count > 0)
    dup2(pipesave[1], 1);
  close_fds(pipefd, pipesave, sh);
  if (!my_strcmp(sh->cmd->command[0], "echo"))
    {
      echo_args(sh->cmd->command);
      return ;
    }
  if (my_builtin_check(sh->cmd->command))
    {
      sh->cmd->command[0] = find_cmd(sh->path, sh->cmd->command[0]);
      execve(sh->cmd->command[0], sh->cmd->command, sh->env);
      fprintf(stderr, "error:%s:command not found.\n", sh->cmd->command[0]);
    }
  sh->exit = 1;
}
