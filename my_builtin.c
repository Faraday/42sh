/*
** my_buitin.c for my_builtin in /home/ledara_f/rendu/PSU_2013_minishell1
** 
** Made by ledara_f
** Login   <ledara_f@epitech.net>
** 
** Started on  Fri Dec 13 14:55:45 2013 ledara_f
** Last update Sun May 25 17:12:34 2014 elkaim_r
*/

#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "sh.h"

void	my_env(char **envp)
{
  int	i;

  i = 0;
  while (envp[i] != NULL)
    {
      my_putstr(envp[i]);
      my_putchar('\n');
      i = i + 1;
    }
}

void	my_bg(char **av)
{
  int	pg;
  int	process;

  if (!g_process_tab || !g_pid_tab || !g_process_nb)
    return ;
  process = size_int(g_pid_tab) - 1;
  if (av[1])
    process = my_getnbr(av[1]);
  if (process > size_int(g_pid_tab) || process <= 0)
    return ;
  if (g_pid_tab && g_pid_tab[process - 1] != -1)
    {
      pg = getpgid(g_pid_tab[process - 1]);
      printf("resuming process %s in background\n",
	     g_process_tab[process - 1]);
      kill(-1 * pg, SIGCONT);
    }
}

void	my_fg(char **av, t_sh *sh)
{
  int	pg;
  int	i;
  int	process;

  i = 0;
  if (!g_process_tab || !g_pid_tab || !g_process_nb)
    return ;
  process = size_int(g_pid_tab) - 1;
  if (av[1])
    process = my_getnbr(av[1]);
  if (process > size_int(g_pid_tab) || process <= 0)
    return ;
  if (g_pid_tab && g_pid_tab[process - 1] != -1)
    {
      pg = getpgid(g_pid_tab[process - 1]);
      printf("resuming process %s\n", g_process_tab[process - 1]);
      tcsetpgrp(0, pg);
      kill(-1 * pg, SIGCONT);
      while (i < g_process_nb[process - 1])
	{
	  waitpid(-1 * pg, &sh->status, WUNTRACED);
	  ++i;
	}
      tcsetpgrp(0, getpgid(0));
    }
}

void	my_jobs(void)
{
  int	i;

  i = 0;
  if (!g_process_tab || !g_pid_tab || !g_process_nb)
    return ;
  while (g_process_tab[i])
    {
      printf("[%d]{%s}\n", i + 1, g_process_tab[i]);
      ++i;
    }
  if (i == 0)
    printf("no current job.\n");
}

void	my_builtin(char **av, char ***envp, t_sh *sh)
{
  int	i;

  i = 1;
  if (!av || !av[0])
    return ;
  if (my_strcmp(av[0], "cd") == 0)
    my_cd(av, envp, sh);
  else if (strcmp(av[0], "setenv") == 0)
    {
      while (av[i] != 0)
	{
	  if ((*envp = my_unsetenv(*envp, av[i])) == NULL)
	    return ;
	  i = i + 1;
	}
      if ((*envp = my_setenv(*envp, &av[1])) == NULL)
	return ;
    }
  else if (strcmp(av[0], "env") == 0)
    my_env(*envp);
  else if (strcmp(av[0], "fg") == 0)
    my_fg(av, sh);
  else if (strcmp(av[0], "bg") == 0)
    my_bg(av);
  builtin_2(av, envp, sh, i);
}
