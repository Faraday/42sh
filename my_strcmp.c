/*
** my_strcmp.c for my_strcmp in /home/elkaim_r/rendu/Piscine-C-Jour_06
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Mon Oct  7 18:29:47 2013 elkaim_r
** Last update Wed May 21 18:04:06 2014 ledara_f
*/

#include "sh.h"

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  if (!s1 || !s2)
    return (-1);
  if (my_strlen(s1) != my_strlen(s2))
    return (1);
  while (s1[i] == s2[i] && s1[i] != 0 && s2[i] != 0)
    i = i + 1;
  return (s1[i] - s2[i]);
}
