/*
** str.c for str in /home/amstut_a/rendu/42sh/debug
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Fri Mar 21 12:34:15 2014 amstut_a
** Last update Thu May 22 08:04:13 2014 amstut_a
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <termios.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "sh.h"

char	*my_strcpy(char *src)
{
  char	*cpy;
  int	i;

  i = 0;
  if (src == NULL)
    return (NULL);
  if ((cpy = malloc(sizeof(char) * (my_strlen(src) + 2))) == NULL)
    return (NULL);
  while (src[i])
    {
      cpy[i] = src[i];
      i++;
    }
  cpy[i] = 0;
  if (i == 0)
    return (NULL);
  return (cpy);
}

char	*cut_chain(char *str, int pos)
{
  int	i;
  char	*rep;

  i = 0;
  if ((rep = malloc(sizeof(char) * (my_strlen(str) + 2))) != NULL)
    {
      while (str[i] && i < pos)
	{
	  rep[i] = str[i];
	  i++;
	}
      rep[i] = 0;
      return (rep);
    }
  else
    return (NULL);
}
