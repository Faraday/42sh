/*
** lib2.c for lib2 in /home/amstut_a/42sh
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Thu May 22 10:25:57 2014 amstut_a
** Last update Sun May 25 11:48:36 2014 elkaim_r
*/

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "sh.h"

void	freex(char **bigtab)
{
  int	i;

  i = 0;
  while (bigtab[i] != NULL)
    {
      free(bigtab[i]);
      i++;
    }
  if (i != 0)
    free(bigtab);
}

char	*my_strcat(char *dest, char *src)
{
  int	i;
  int	j;
  int	size;
  char	*rep;

  size = my_strlen(dest) + my_strlen(src);
  if (!dest || !src || (rep = xmalloc(sizeof(char) * (size + 4))) == NULL)
    return (NULL);
  i = 0;
  j = 0;
  if (dest != NULL)
    while (dest[i] != '\0')
      {
	rep[i] = dest[i];
	i = i + 1;
      }
  if (src != NULL)
    while (src[j] != '\0')
      {
	rep[i] = src[j];
	i = i + 1;
	j = j + 1;
      }
  rep[i] = '\0';
  return (rep);
}

char	*str_insert(char *d, char *s, int nb)
{
  int	i;
  int	j;
  int	k;
  char	*res;

  i = 0;
  j = 0;
  k = 0;
  if (!d || !s ||
      (res = xmalloc(sizeof(char) * (strlen(d) + strlen(s) + 1))) == NULL)
    return (NULL);
  while (d[k] != 0 && k < nb)
    res[i++] = d[k++];
  while (s[j] != 0)
    res[i++] = s[j++];
  while (d[k] != 0)
    res[i++] = d[k++];
  res[i] = 0;
  return (res);
}

void	*xmalloc(size_t size)
{
  void	*pointer;

  pointer = malloc(size);
  if (!pointer)
    my_puterror("error:memory allocation failed\n");
  return (pointer);
}
