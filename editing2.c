/*
** editing2.c for  in /home/amstut_a/rendu/42sh/debug
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Wed Apr  2 10:28:40 2014 amstut_a
** Last update Thu May 22 07:58:06 2014 amstut_a
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <termios.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "sh.h"

void	my_move(char *s, int *pos, t_flag *flags)
{
  if (s[2] == 68)
    {
      *pos = *pos - 1;
      flags->left_key = 1;
      go_left();
    }
  else if (s[2] == 67)
    {
      *pos = *pos + 1;
      flags->right_key = 1;
      go_right();
    }
}

void	add_letter(char *s, char **line, int *pos)
{
  if (*line != NULL)
    *line = my_strcat(*line, s);
  else
    *line = s;
  *pos = *pos + 2;
}

void	get_down(void)
{
  char	*str;

  str = tgetstr("do", NULL);
  if (str != NULL)
    my_putstr(str);
}

void	get_high(int width)
{
  char	*str;

  str = tgetstr("up", NULL);
  if (str != NULL)
    my_putstr(str);
  while (width > 0)
    {
      go_right();
      width--;
    }
}

void	get_high_left(int width)
{
  char	*str;

  str = tgetstr("up", NULL);
  if (str != NULL)
    my_putstr(str);
  while (width > 0)
    {
      go_left();
      width--;
    }
}
