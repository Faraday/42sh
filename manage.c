/*
** manage.c for manage in /home/elkaim_r/rendu/PSU_2013_minishell2
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Fri Feb 28 15:23:39 2014 elkaim_r
** Last update Sun May 25 17:09:10 2014 elkaim_r
*/

#include <stdlib.h>
#include <string.h>
#include "sh.h"

int	is_sep(char *str, int i)
{
  if (i < my_strlen(str))
    {
      if (str[i] == ';' || my_strncmp(&str[i], "||", 2) == 0 ||
	  my_strncmp(&str[i], "&&", 2) == 0)
	return (1);
      if (i > 0 && my_strncmp(&str[i - 1], "||", 2) == 0)
	return (1);
      if (i > 0 && my_strncmp(&str[i - 1], "&&", 2) == 0)
	return (1);
    }
  return (0);
}

void	epure_loop(char **command, int *i, int *j)
{
  int	inc;
  int	rd;

  if ((*command)[*i] == '\t')
    (*command)[*i] = ' ';
  if ((*command)[*i] == ' ')
    *j += 1;
  if ((rd = get_rdi_value(&(*command)[*i])) != 0 ||
      ((*command)[*i] == '|' && (*command)[*i + 1] != '|'
       && *i > 0 && (*command)[*i - 1] != '|'))
    {
      if (rd == 2 || rd == 4)
	inc = 2;
      else
	inc = 1;
      *command = str_insert(*command, " ", *i);
      if (my_strlen(*command) > *i + 1 + inc)
	{
	  *command = str_insert(*command, " ", *i + 1 + inc);
	  *i += 2 + inc - 1;
	}
      else
	*i += 1;
    }
}

char	*epure_command(char *command, int i, int j)
{
  while (i < my_strlen(command) && command[i] != 0)
    {
      epure_loop(&command, &i, &j);
      ++i;
    }
  if (j == (my_strlen(command)))
    command[0] = 0;
  return (command);
}

void	manage_command(char **command)
{
  if (command[0] != NULL)
    {
      command[0] = epure_command(command[0], 0, 0);
      to_history(command[0]);
    }
}
