/*
** pipe.c for 42sh in /home/elkaim_r/Documents/42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Fri May 23 13:58:02 2014 elkaim_r
** Last update Sun May 25 17:08:06 2014 elkaim_r
*/

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sh.h"
#include "get_next_line.h"

void	init_pipes(int pipefd[2], int pipesave[2], pid_t *pg)
{
  pipefd[0] = -1;
  pipefd[1] = -1;
  pipesave[0] = -1;
  pipesave[1] = -1;
  *pg = -1;
}

void	close_pipes(int pipefd[2], int pipesave[2], t_sh *sh)
{
  close(pipesave[1]);
  close(pipefd[0]);
  pipesave[0] = pipefd[0];
  pipesave[1] = pipefd[1];
  sh->cmd = sh->cmd->next;
}

int	new_process(t_sh *sh, pid_t *pg, int idx, t_pipe *pipes)
{
  if ((sh->res = fork()) == -1)
    {
      fprintf(stderr, "error:couldn't create child process\n");
      return (0);
    }
  if (!sh->res)
    {
      child_exec(sh, idx, pipes->pipefd, pipes->pipesave);
      return (-1);
    }
  if (*pg == -1)
    {
      *pg = sh->res;
      setpgid(sh->res, 0);
    }
  else
    setpgid(sh->res, *pg);
  close_pipes(pipes->pipefd, pipes->pipesave, sh);
  return (0);
}
