/*
** continue.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Thu Mar 27 11:54:44 2014 elkaim_r
** Last update Wed May 21 17:50:57 2014 ledara_f
*/

#include "sh.h"

int	must_continue(t_sh *sh, int i)
{
  if (i < 0)
    return (1);
  if (sh->order[i] == OR && sh->status == 0)
    return (0);
  if (sh->order[i] == AND && sh->status != 0)
    return (0);
  return (1);
}
