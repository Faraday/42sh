/*
** disp.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Sun Mar 23 11:31:54 2014 elkaim_r
** Last update Sun May 25 10:40:31 2014 elkaim_r
*/

#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
#include "sh.h"

char	*get_prompt(void)
{
  char	*prompt;
  char	*directory;

  prompt = my_strcpy("\033[034;1m");
  prompt = my_strcat(prompt, "42sh");
  prompt = my_strcat(prompt, "\033[0m");
  prompt = my_strcat(prompt, " ");
  directory = getcwd(NULL, 4096);
  if (directory)
    {
      prompt = my_strcat(prompt, "\033[032;1m");
      prompt = my_strcat(prompt, directory);
      prompt = my_strcat(prompt, "\033[0m");
    }
  prompt = my_strcat(prompt, "$ ");
  return (prompt);
}
