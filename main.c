/*
** main.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Thu Mar 13 11:38:13 2014 elkaim_r
** Last update Fri May 23 13:00:57 2014 ledara_f
*/

#include <stdlib.h>
#include <signal.h>
#include "sh.h"

char	**g_process_tab = NULL;
int	*g_pid_tab = NULL;
int	*g_process_nb = NULL;

void	init_sh(t_sh *sh, char **env)
{
  sh->exit = 0;
  sh->env = get_env(env);
  sh->path = NULL;
  sh->alias = malloc(1 * sizeof(char **));
  if (sh->alias)
    sh->alias[0] = NULL;
  sh->local = malloc(1 * sizeof(char **));
  if (sh->local)
    sh->local[0] = NULL;
  g_process_tab = malloc(1 * sizeof(char **));
  if (g_process_tab)
    g_process_tab[0] = NULL;
  g_pid_tab = malloc(1 * sizeof(int));
  if (g_pid_tab)
    g_pid_tab[0] = -1;
  g_process_nb = malloc(1 * sizeof(int));
  if (g_process_nb)
    g_process_nb[0] = -1;
  sh->instruct = NULL;
  sh->home = NULL;
  sh->status = 0;
  sh->stop = 0;
  sh->fd_in = -1;
  sh->fd_out = -1;
  sh->bg = 0;
}

int	main(int ac, char **av, char **env)
{
  t_sh	sh;
  int	exitvalue;

  (void)ac;
  (void)av;
  init_sh(&sh, env);
  signal(SIGTTOU, SIG_IGN);
  signal(SIGTSTP, SIG_IGN);
  signal(SIGINT, SIG_IGN);
  signal(SIGCHLD, check_process);
  exitvalue = prompter(&sh);
  return (exitvalue);
}
