/*
** var.c for var in /home/ledara_f/test/var42
** 
** Made by ledara_f
** Login   <ledara_f@epitech.net>
** 
** Started on  Tue Apr 29 14:00:00 2014 ledara_f
** Last update Sun May 25 11:50:14 2014 elkaim_r
*/

#include <stdlib.h>
#include "sh.h"

int	is_alpha_num(char c)
{
  if (c >= 'a' && c <= 'z')
    return (1);
  else if (c >= 'A' && c <= 'Z')
    return (1);
  else if (c >= '0' && c <= '9')
    return (1);
  else
    return (0);
}

int	parse(char **local, char *str, char *input, int *k)
{
  int	i;
  int	j;

  i = 0;
  while (local[i] != NULL)
    {
      j = -1;
      while (local[i][++j] != '=');
      if (my_strlen(str) != j)
	i++;
      else
	{
	  if (my_strncmp(local[i], str, j) == 0)
	    {
	      while (local[i][++j] != 0)
		{
		  input[*k] = local[i][j];
		  *k = *k + 1;
		}
	      return (0);
	    }
	  i++;
	}
    }
  return (1);
}

int	search_long(char **local, char *rez, int *longueur)
{
  int	i;
  int	j;

  i = -1;
  while (local[++i])
    {
      j = -1;
      while (local[i][++j] != '=');
      if (my_strlen(rez) == j)
	{
	  if (my_strncmp(local[i], rez, j) == 0)
	    {
	      *longueur = *longueur + my_strlen(local[i]);
	      return (1);
	    }
	}
    }
  return (0);
}

int	malloc_my(char **input, char *str, char **local, char **envp)
{
  int	longueur;
  char	rez[4096];
  int	i;
  int	c;

  i = -1;
  longueur = 0;
  while (str[++i] != 0)
    {
      if (str[i] == '$')
	{
	  c = 0;
	  while (is_alpha_num(str[++i]) == 1 && str[i] != 0)
	    rez[c++] = str[i];
	  rez[c] = 0;
	  if ((search_long(envp, rez, &longueur)) == 1)
	    search_long(local, rez, &longueur);
	}
      if (str[i] == 0)
	break ;
      longueur++;
    }
  if ((*input = xmalloc((longueur * 2) * sizeof(char))) == NULL)
    return (-1);
  return (0);
}

char	*shell_var(char *str, char **local, char **envp, int j)
{
  char	*rez;
  char	*input;
  int	i;
  int	c;

  i = 0;
  if ((malloc_my(&input, str, local, envp)) == -1 ||
      ((rez = xmalloc((my_strlen(str) + 1) * sizeof(char))) == NULL))
    return (str);
  while (str[i] != 0)
    {
      if (str[i] == '$')
	{
	  c = 0;
	  while (is_alpha_num(str[++i]) == 1 && str[i] != 0)
	    rez[c++] = str[i];
	  rez[c] = 0;
	  if ((parse(local, rez, input, &j)) == 1)
	    parse(envp, rez, input, &j);
	}
      else
	input[j++] = str[i++];
    }
  input[j] = 0;
  return (input);
}
