/*
** vader.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Thu Mar 13 13:56:10 2014 elkaim_r
** Last update Sat May 24 16:10:01 2014 elkaim_r
*/

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include "sh.h"

void		disp_signal(int status)
{
  int		value;

  if (WIFSIGNALED(status))
    {
      value = WTERMSIG(status);
      if (value == 11)
	printf("segmentation fault :'(\n");
    }
}

void		call_me_daddy(t_sh *sh, pid_t res, int i, int idx)
{
  int		size;

  size = size_list(sh->cmd);
  if (!sh->bg)
    {
      tcsetpgrp(0, res);
      while (idx < size)
	{
	  waitpid(-1 * res, &sh->status, WUNTRACED);
	  disp_signal(sh->status);
	  ++idx;
	}
      tcsetpgrp(0, getpgid(0));
    }
  if ((WIFSTOPPED(sh->status) || sh->bg) && g_process_tab && g_pid_tab
      && g_process_nb)
    {
      append_int(&g_pid_tab, res);
      append_tab(&g_process_tab, sh->instruct[i]);
      append_int(&g_process_nb, size);
      if (WIFSTOPPED(sh->status))
	printf("[%s]:suspended\n", sh->instruct[i]);
    }
  return ;
}
