/*
** main.c for main in /home/amstut_a/rendu/42sh
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Thu Mar 13 11:01:26 2014 amstut_a
** Last update Fri May 23 12:56:54 2014 ledara_f
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "sh.h"

int		word_count(char *str, char s)
{
  int		c;
  int		i;

  c = 0;
  i = 0;
  while (str[i] == ' ')
    i++;
  while (str[i] != 0)
    {
      if ((str[i] == s && str[i + 1] != s) || str[i + 1] == 0)
	c++;
      i++;
    }
  return (c);
}

char		*linefill(char *str, int *i, char s)
{
  int		a;
  char		*line;

  a = *i;
  while (str[a] != s && str[a] != 0)
    a++;
  if ((line = malloc(sizeof(char) * (a + 1))) == NULL)
    return (NULL);
  a = 0;
  while (str[*i] != s && str[*i] != 0)
    {
      line[a] = str[*i];
      a++;
      *i = *i + 1;
    }
  line[a] = 0;
  return (line);
}

char		**my_str_to_wordtab(char *str, char s)
{
  char		**bigtab;
  int		k;
  int		i;
  int		j;

  k = 0;
  i = 0;
  j = 0;
  if (str == NULL)
    return (NULL);
  k = word_count(str, s);
  if ((bigtab = malloc(sizeof(char **) * (k + 1))) == NULL)
    return (NULL);
  while (str[i] == ' ' || str[i] == '\t')
    ++i;
  while (j < k)
    {
      bigtab[j] = linefill(str, &i, s);
      while (str[i] == s)
	i++;
      j++;
    }
  bigtab[j] = NULL;
  return (bigtab);
}

char		**get_path(char **env)
{
  char		*tmp;
  char		**path;
  int		i;

  if (env == NULL)
    return (NULL);
  i = 0;
  while (env[i] != NULL && strncmp(env[i], "PATH", 4) != 0)
    i++;
  if (env[i] == NULL)
    {
      path = my_str_to_wordtab("-null", ' ');
      return (path);
    }

  if ((tmp = malloc(sizeof(char) * strlen(env[i]))) == NULL)
    return (NULL);
  tmp = strcpy(tmp, &env[i][5]);
  path = my_str_to_wordtab(tmp, ':');
  return (path);
}

char		*find_cmd(char **path, char *input)
{
  struct stat	b;
  char		*cmd;
  int		i;

  i = 0;
  if (path == NULL)
    return (input);
  while (path[i])
    {
      cmd = my_strcat(path[i], "/");
      cmd = my_strcat(cmd, input);
      if (stat(cmd, &b) == 0)
	return (cmd);
      i++;
    }
  return (input);
}
