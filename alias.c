/*
** main.c for main.c in /home/ledara_f/test/alias
** 
** Made by ledara_f
** Login   <ledara_f@epitech.net>
** 
** Started on  Wed Apr  2 11:58:46 2014 ledara_f
** Last update Sun May 25 11:50:07 2014 elkaim_r
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "get_next_line.h"
#include "sh.h"

int	alias2(char ***alias, char *str)
{
  *alias = my_unsetenv(*alias, str);
  append_tab(alias, str);
  return (0);
}

char	*alias42(char *argv, char ***tabalias)
{
  char	*str;

  if (my_strncmp(argv, "alias ", 6) == 0)
    alias2(tabalias, &argv[6]);
  else
    {
      str = alias(*tabalias, argv);
      return (str);
    }
  return (0);
}

char	*replace(char *str, char **tabalias, int *o)
{
  char	*rez;
  int	i;
  int	j;
  int	k;

  i = 0;
  k = 0;
  *o = 0;
  while (tabalias[i])
    {
      j = 0;
      while (tabalias[i][j] != 0 && tabalias[i][j] != '=')
	j++;
      if (my_strncmp(str, tabalias[i], j++) == 0)
	{
	  if ((rez = xmalloc((my_strlen(tabalias[i]) + 1) * sizeof(char))) == NULL)
	    return (str);
	  while (tabalias[i][j])
	    rez[k++] = tabalias[i][j++];
	  rez[k] = 0;
	  return (rez);
	}
      i++;
    }
  return (str);
}

char	*first_alias2(char *str, char **tabalias, char *word, char *rez)
{
  int	i;
  int	j;
  int	k;

  i = 0;
  j = 0;
  while (str[i] != 0)
    {
      k = 0;
      if (str[i] == '|')
	{
	  rez[j++] = str[i++];
	  while (str[i] == ' ' && str[i] != 0)
	    i++;
	  while ((is_alpha(str[i])) == 1 && str[i] != 0)
	    word[k++] = str[i++];
	  word[k] = 0;
	  word = replace(word, tabalias, &k);
	  while (word[k])
	    rez[j++] = word[k++];
	}
      rez[j++] = str[i++];
    }
  rez[j] = 0;
  return (rez);
}

char	*first_alias(char *str, char **tabalias)
{
  char	*rez;
  char	*word;

  if ((rez = xmalloc((my_strlen(str) + 4096) * sizeof(char))) == NULL)
    return (str);
  if ((word = xmalloc((my_strlen(str)) * sizeof(char))) == NULL)
    return (str);
  rez = first_alias2(str, tabalias, word, rez);
  return (rez);
}
