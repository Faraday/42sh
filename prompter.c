/*
** prompter.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Thu Mar 13 11:58:32 2014 elkaim_r
** Last update Sun May 25 10:53:11 2014 elkaim_r
*/

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sh.h"
#include "get_next_line.h"

int		execute(t_sh *sh, int i)
{
  pid_t		pg;
  t_cmd		*tmp;
  int		idx;
  t_pipe	pipes;

  idx = 0;
  init_pipes(pipes.pipefd, pipes.pipesave, &pg);
  tmp = sh->cmd;
  while (sh->cmd)
    {
      if (sh->cmd->next)
	pipe(pipes.pipefd);
      my_builtin(sh->cmd->command, &sh->env, sh);
      if (new_process(sh, &pg, idx, &pipes) == -1)
	return (-1);
      ++idx;
    }
  sh->cmd = tmp;
  call_me_daddy(sh, pg, i, 0);
  return (0);
}

int	exec_loop(t_sh *sh)
{
  int	i;

  i = 0;
  if (!sh->instruct)
    return (-1);
  while (sh->instruct[i] != NULL)
    {
      parse_alias(sh, i);
      if (sh->instruct[i] && must_continue(sh, i - 1) == 1)
	if (exec_arg(sh, i))
	  return (0);
      close(sh->fd_in);
      sh->fd_in = -1;
      close(sh->fd_out);
      sh->fd_out = -1;
      sh->bg = 0;
      ++i;
    }
  return (0);
}

int	get_args(t_sh *sh)
{
  char	*prompt;

  prompt = get_prompt();
  sh->input = line_editor(prompt);
  if (!sh->input)
    return (-1);
  if (my_strlen(sh->input) == 0)
    return (-2);
  double_quote(sh, '\'');
  if (sh->local && sh->env)
    sh->input = shell_var(sh->input, sh->local, sh->env, 0);
  if (sh->input && my_strlen(sh->input) >= 4096)
    sh->input[4096] = 0;
  double_quote(sh, '"');
  if (sh->alias)
    sh->input = first_alias(sh->input, sh->alias);
  manage_command(&sh->input);
  if (!sh->input || check_errors(sh->input))
    return (-2);
  sh->instruct = my_str_to_big_tab(sh->input, &sh->order);
  if (!sh->instruct)
    return (-2);
  return (0);
}

int	prompter(t_sh *sh)
{
  int	i;

  while (sh->stop == 0)
    {
      sh->path = get_path(sh->env);
      if ((i = get_args(sh)) == 0)
	{
	  if (exec_loop(sh) == -1)
	    return (sh->exit);
	}
      else if (i == -1)
	break ;
    }
  return (sh->exit);
}
