/*
** supertab.c for supertab in /home/elkaim_r/rendu/PSU_2013_minishell1
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Mon Feb  3 10:51:27 2014 elkaim_r
** Last update Sun May 25 11:50:13 2014 elkaim_r
*/

#include <stdlib.h>
#include <string.h>
#include "sh.h"

char	*fillcommand(char *str, int *i)
{
  int	a;
  char	*line;

  a = *i;
  while (is_sep(str, a) == 0 && str[a] != 0)
    a++;
  if ((line = xmalloc(sizeof(char) * (a + 1))) == NULL)
    return (NULL);
  a = 0;
  while (is_sep(str, *i) == 0 && str[*i] != 0)
    {
      line[a] = str[*i];
      a = a + 1;
      *i = *i + 1;
    }
  line[a] = 0;
  return (line);
}

int	command_count(char *str)
{
  int	c;
  int	i;

  c = 0;
  i = 0;
  while (str[i] == ' ')
    i++;
  while (str[i] != 0)
    {
      if ((is_sep(str, i) == 1 && is_sep(str, i + 1) == 0) || str[i + 1] == 0)
	c++;
      i++;
    }
  return (c);
}

void	get_commands(Separators **order, char *str)
{
  int	i;
  int	k;

  i = 0;
  k = 0;
  while (str[i] == ' ')
    i++;
  while (str[i] != 0)
    {
      if (i > 0 && is_sep(str, i) == 1 && is_sep(str, i - 1) == 0)
	{
	  if (str[i] == ';')
	    (*order)[k] = NONE;
	  else if (str[i] == '|')
	    (*order)[k] = OR;
	  else if (str[i] == '&')
	    (*order)[k] = AND;
	  ++k;
	}
      ++i;
    }
}

char	**my_str_to_command_tab(char *str, Separators **order, int i, int j)
{
  int	c;
  char	**bigtab;

  c = 0;
  if (str == NULL)
    return (NULL);
  c = command_count(str);
  if ((*order = xmalloc(sizeof(*order) * (c))) == NULL)
    return (NULL);
  get_commands(order, str);
  if ((bigtab = xmalloc(sizeof(char **) * (c + 1))) == NULL)
    return (NULL);
  while (str[i] == ' ')
    ++i;
  while (j < c)
    {
      bigtab[j] = fillcommand(str, &i);
      while (is_sep(str, i) == 1)
	i++;
      j++;
    }
  bigtab[j] = NULL;
  return (bigtab);
}

char	**my_str_to_big_tab(char *arg, Separators **order)
{
  char	**res1;

  res1 = my_str_to_command_tab(arg, order, 0, 0);
  return (res1);
}
