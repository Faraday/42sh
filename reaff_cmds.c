/*
** reaff_cmds.c for reaff_cmds in /home/amstut_a/rendu/42sh/debug
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Thu Apr 10 11:17:19 2014 amstut_a
** Last update Thu May 22 08:03:16 2014 amstut_a
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <termios.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "sh.h"

void	my_replace_cursor(int pos_bck, char *line, int width, t_flag *flags)
{
  int	i;

  i = my_strlen(line);
  while (i >= pos_bck)
    {
      if (i == width && flags->cur_lines > 0 && flags->total_lines > 0)
	{
	  get_high(width);
	  if (flags->cur_lines > 0)
	    flags->cur_lines = 0;
	}
      else
	go_left();
      --i;
    }
}

char	*get_next_word(char *line, int pos)
{
  char	*res;
  int	length;
  int	i;

  i = 0;
  length = my_strlen(line);
  if ((res = malloc(sizeof(char) * (length + 2))) == NULL)
    return (NULL);
  while (line[pos] != 0 && line[pos] != ' ' && line[pos] != '\t')
    {
      res[i] = line[pos];
      i++;
      pos++;
    }
  res[i] = '\0';
  return (res);
}

void	reaff_line(t_flag *flags, char *line, int *pos, int width)
{
  if (flags->autocomplete == 2)
    reaff_autocomplete(line, *pos, flags, width);
  else if (flags->total_lines == 0)
    reaff_cmd(line, pos, flags, width);
  else if (flags->total_lines > 0 && flags->cur_lines == 0)
    {
      if (my_strlen(line) - 1 < width)
	flags->total_lines = 0;
      reaff_multilines_up(line, pos, flags, width);
    }
  else if ((flags->total_lines > 0 && flags->cur_lines > 0))
    reaff_multilines_down(line, pos, flags, width);
}
