/*
** redi.c for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Thu Apr  3 13:36:09 2014 elkaim_r
** Last update Sun May 25 11:50:12 2014 elkaim_r
*/

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "sh.h"

void		get_cmd_pipes(t_sh *sh)
{
  int		i;
  char		**buffer;

  i = 0;
  buffer = xmalloc(1 * sizeof(char *));
  if (buffer)
    buffer[0] = NULL;
  sh->cmd = NULL;
  while (sh->command[i])
    {
      if (my_strcmp(sh->command[i], "|") && buffer)
	append_tab(&buffer, sh->command[i]);
      else if (buffer)
	{
	  add_to_list(&sh->cmd, buffer);
	  buffer = xmalloc(1 * sizeof(char *));
	  if (buffer)
	    buffer[0] = NULL;
	}
      ++i;
    }
  if (buffer && buffer[0])
    add_to_list(&sh->cmd, buffer);
}

void		get_redirect(char *str, int value, t_sh *sh)
{
  static void	(*redirects[4])(t_sh *sh, char *str) =
    {trunc_file, forward_file, get_file, get_input};

  if (value >= 0 && value <= 3)
    redirects[value](sh, str);
}

int		is_redi(char *str)
{
  if (!my_strncmp(str, ">>", 2) || !my_strncmp(str, "<<", 2)
      || !my_strncmp(str, ">", 1) || !my_strncmp(str, "<", 1))
    return (1);
  return (0);
}

int		get_rdi_value(char *str)
{
  if (!my_strncmp(str, ">>", 2))
    return (2);
  if (!my_strncmp(str, "<<", 2))
    return (4);
  if (!my_strncmp(str, ">", 1))
    return (1);
  if (!my_strncmp(str, "<", 1))
    return (3);
  return (0);
}

char		**redi_command(t_sh *sh, int idx)
{
  int		i;
  char		**res;
  char		**final;

  i = 0;
  res = my_str_to_wordtab(sh->instruct[idx], ' ');
  final = xmalloc(sizeof(char **) * 1);
  if (!final)
    return (NULL);
  final[0] = NULL;
  while (res[i])
    {
      if (!is_redi(res[i])
	  && ((i > 0 && is_redi(res[i - 1]) == 0)
	      || (i == 0 && is_redi(res[i]) == 0))
	  && my_strcmp(res[i], "&"))
	globber(&final, res[i]);
      else if (i > 0 && is_redi(res[i - 1]))
	get_redirect(res[i], get_rdi_value(res[i - 1]) - 1, sh);
      else if (!my_strcmp(res[i], "&"))
	sh->bg = 1;
      ++i;
    }
  return (final);
}
