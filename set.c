/*
** my_setenv.c for setenv in /home/ledara_f/rendu/PSU_2013_minishell1
** 
** Made by ledara_f
** Login   <ledara_f@epitech.net>
** 
** Started on  Fri Dec 13 14:44:02 2013 ledara_f
** Last update Wed May 21 17:57:18 2014 ledara_f
*/

#include <stdlib.h>
#include "sh.h"

char	**my_set(char **local, char **str)
{
  local = my_unsetenv(local, str[0]);
  local = my_setenv(local, str);
  return (local);
}
