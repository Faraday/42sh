/*
** remove_prompt.c for remove_prompt in /home/amstut_a/42sh/autocomplete
** 
** Made by amstut_a
** Login   <amstut_a@epitech.net>
** 
** Started on  Tue May 13 11:46:32 2014 amstut_a
** Last update Tue May 13 11:51:18 2014 amstut_a
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <termios.h>
#include <ncurses/curses.h>
#include <term.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "sh.h"

char	*remove_prompt(char *line, char *prompt)
{
  char	*res;
  int	len;
  int	i;
  int	j;

  i = 0;
  j = 0;
  len = my_strlen(line);
  if ((res = calloc(len + 1, sizeof(char))) == NULL)
    return (NULL);
  while (line[i] && line[i] == prompt[i])
    ++i;
  while (line[i])
    {
      res[j] = line[i];
      ++i;
      ++j;
    }
  res[j] = 0;
  return (res);
}
