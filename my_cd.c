/*
** my_cd.c for my_cd in /home/ledara_f/rendu/PSU_2013_minishell1
** 
** Made by ledara_f
** Login   <ledara_f@epitech.net>
** 
** Started on  Fri Dec 13 14:07:18 2013 ledara_f
** Last update Fri May 23 13:04:11 2014 ledara_f
*/

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "sh.h"

char	**my_setpwd_pwd(char **envp)
{
  char	*old;
  char	**tmp;

  if ((old = malloc(4096 * sizeof(char))) == NULL)
    return (envp);
  if ((old = getcwd(old, 4096)) == NULL)
    return (envp);
  envp = my_unsetenv(envp, "PWD");
  if ((tmp = malloc(3 * sizeof(char **))) == NULL)
    return (envp);
  if ((tmp[0] = calloc(4096, sizeof(char))) == NULL)
    return (envp);
  tmp[0] = my_strcat(tmp[0], "PWD");
  tmp[1] = old;
  tmp[2] = 0;
  envp = my_setenv(envp, tmp);
  return (envp);
}

int	my_tilde_minus(char **argv, char ***envp)
{
  char	*home;
  char	*path;

  home = NULL;
  if (argv[1][0] == '~')
    {
      home = my_recup_home(*envp, home);
      path = my_strcat(&argv[1][1], home);
      if ((*envp = my_setpwd_old(*envp)) == NULL)
	return (-1);
      chdir(path);
      if ((*envp = my_setpwd_pwd(*envp)) == NULL)
	return (-1);
      return (0);
    }
  home = my_recup_oldpwd(*envp, home);
  my_putstr(home);
  my_putstr("\n");
  if ((*envp = my_setpwd_old(*envp)) == NULL)
    return (-1);
  chdir(home);
  if ((*envp = my_setpwd_pwd(*envp)) == NULL)
    return (-1);
  return (0);
}

int	my_cd2(char ***envp)
{
  char	*home;
  int	i;

  home = NULL;
  home = my_recup_home(*envp, home);
  if ((*envp = my_setpwd_old(*envp)) == NULL)
    return (-1);
  i = chdir(home);
  if (i == -1)
    my_cannot_acces(home, 1);
  if ((*envp = my_setpwd_pwd(*envp)) == NULL)
    return (-1);
  return (0);
}

int	my_cd3(char **argv, char ***envp, t_sh *sh)
{
  int	i;

  if (argv[1][0] == '~' || argv[1][0] == '-')
    my_tilde_minus(argv, envp);
  else
    {
      if ((*envp = my_setpwd_old(*envp)) == NULL)
	return (-1);
      if ((i = chdir(argv[1])) == -1)
	{
	  my_cannot_acces(argv[1], 0);
	  sh->status = 1;
	}
      else
	sh->status = 0;
      if ((*envp = my_setpwd_pwd(*envp)) == NULL)
	return (-1);
    }
  return (0);
}

int	my_cd(char **argv, char ***envp, t_sh *sh)
{
  if (strcmp(argv[0], "cd") == 0)
    {
      if (argv[1] != 0)
	{
	  if ((my_cd3(argv, envp, sh)) == -1)
	    return (-1);
	  else
	    return (0);
	}
      else
	if ((my_cd2(envp)) == -1)
	  return (-1);
      return (0);
    }
  return (1);
}
