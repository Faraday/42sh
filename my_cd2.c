/*
** my_cd2.c for qsdqsd in /home/ledara_f/rendu/PSU_2013_minishell2
** 
** Made by ledara_f
** Login   <ledara_f@epitech.net>
** 
** Started on  Thu Mar 13 14:44:42 2014 ledara_f
** Last update Sun May 25 11:50:11 2014 elkaim_r
*/

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "sh.h"

char	**my_setpwd_old(char **envp)
{
  char	*old;
  char	**tmp;

  if ((old = xmalloc(4096 * sizeof(char ))) == NULL)
    return (envp);
  if ((old = getcwd(old, 4096)) == NULL)
    return (envp);
  envp = my_unsetenv(envp, "OLDPWD");
  if ((tmp = xmalloc(3 * sizeof(char **))) == NULL)
    return (envp);
  if ((tmp[0] = calloc(4096, sizeof(char))) == NULL)
    return (envp);
  tmp[0] = my_strcat(tmp[0], "OLDPWD");
  tmp[1] = old;
  tmp[2] = 0;
  envp = my_setenv(envp, tmp);
  return (envp);
}

char	*my_recup_home(char **envp, char *home)
{
  int	i;

  i = 0;
  while (envp[i] != NULL)
    {
      if (envp[i][0] == 'H' && envp[i][1] == 'O' &&
	  envp[i][2] == 'M' && envp[i][3] == 'E')
	{
	  home = &envp[i][5];
	  break ;
	}
      i = i + 1;
    }
  if (envp[i] == NULL)
    home = "NOT SET";
  return (home);
}

char	*my_recup_oldpwd(char **envp, char *old)
{
  int	i;

  i = 0;
  while (envp[i] != NULL)
    {
      if (envp[i][0] == 'O' && envp[i][1] == 'L' &&
	  envp[i][2] == 'D' && envp[i][3] == 'P')
	{
	  old = &envp[i][7];
	  break ;
	}
      i = i + 1;
    }
  if (envp[i] == NULL)
    old = "NOT SET";
  return (old);
}

void	my_cannot_acces(char *path, int a)
{
  if (a == 0)
    {
      my_puterror("Cannot access : ");
      my_puterror(path);
      if (access(path, F_OK) == -1)
	fprintf(stderr, ": directory does not exist\n");
      else if (access(path, R_OK) == -1)
	fprintf(stderr, ": permission denied\n");
    }
  else if (a == 1)
    my_puterror("ERROR : HOME NOT SET\n");
}
