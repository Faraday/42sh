/*
** 42sh.h for 42sh in /home/elkaim_r/rendu/PSU_2013_42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Thu Mar 13 11:38:46 2014 elkaim_r
// Last update Sun May 25 17:32:34 2014 elkaim_r
*/

#ifndef SH_H_
# define SH_H_

#include <sys/types.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <signal.h>

#define BLUE "\033[1;34m"
#define END "\033[0m"

extern char	**g_process_tab;
extern int	*g_pid_tab;
extern int	*g_process_nb;

typedef enum	Separator
  {
    OR, AND, NONE
  }		Separators;

typedef struct	s_cmd
{
  char		**command;
  struct s_cmd	*next;
}		t_cmd;

typedef struct	s_sh
{
  char		**local;
  char		**env;
  char		**path;
  char		**alias;
  char		**instruct;
  char		**command;
  char		*input;
  char		*home;
  Separators	*order;
  int		exit;
  int		stop;
  int		status;
  int		fd_in;
  int		fd_out;
  int		bg;
  t_cmd		*cmd;
  pid_t		res;
}		t_sh;

typedef struct	s_list
{
  char		*rep;
  struct s_list	*next;
}		t_list;

typedef struct	s_pipe
{
  int	pipefd[2];
  int	pipesave[2];
}		t_pipe;

typedef struct	s_flag
{
  char		*prompt;
  int		suppr;
  int		left_key;
  int		right_key;
  int		autocomplete;
  int		mid_write;
  int		add_letter;
  int		cur_lines;
  int		total_lines;
}		t_flag;

void		freex(char **bigtab);
int		is_alpha(char c);
int		must_continue(t_sh *sh, int i);
int		word_count(char *str, char s);
char		*linefill(char *str, int *i, char s);
char		**my_str_to_wordtab(char *str, char s);
char		**get_path(char **env);
char		*find_cmd(char **path, char *input);
void		trunc_file(t_sh *sh, char *str);
void		forward_file(t_sh *sh, char *str);
void		get_file(t_sh *sh, char *str);
void		get_input(t_sh *sh, char *str);
char		**glob_star(char *);
void		my_memset(char *str, char c, int i);
void		my_putchar(char c);
int		my_strlen(char *str);
void		my_putstr(char *str);
void		my_puterror(char *str);
void		init_sh(t_sh *sh, char **env);
void		my_env(char **envp);
void		my_bg(char **av);
void		my_fg(char **av, t_sh *sh);
void		my_jobs(void);
void		my_builtin(char **av, char ***envp, t_sh *sh);
char		**get_env(char **env);
char		*my_strcat(char *d, char *s);
char		*str_insert(char *d, char *s, int nb);
int		execute(t_sh *sh, int i);
int		exec_loop(t_sh *sh);
int		get_args(t_sh *sh);
int		prompter(t_sh *sh);
char		**my_set(char **local, char **str);
char		*fillcommand(char *str, int *i);
int		command_count(char *str);
void		get_commands(Separators **order, char *str);
char		**my_str_to_command_tab(char *str,
					Separators **order,
					int i,
					int j);
char		**my_str_to_big_tab(char *arg, Separators **order);
int		my_strncmp2(char *s1, char *s2, int n);
int		is_alpha_num(char c);
int		parse(char **local, char *str, char *input, int *k);
char		*shell_var(char *str, char **local, char **envp, int j);
int		*malloc_static(void);
char		*recup_str(char *str, char *argv);
void		skip(int *i, int *exclued);
char		*alias_loop(char **tabalias,
			    char *str,
			    int **exclued,
			    char *argv);
char		*alias(char **tabalias, char *argv);
int		size_tab(char **);
int		size_int(int *);
void		aff_tab(char **);
void		append_tab(char ***, char *string);
void		append_int(int **, int new);
char		*get_prompt(void);
int		exec_arg(t_sh *sh, int i);
int		to_history(char *str);
int		size_list(t_cmd *cmd);
void		add_to_list(t_cmd **list, char **command);
char		**my_setpwd_old(char **envp);
char		*my_recup_home(char **envp, char *home);
char		*my_recup_oldpwd(char **envp, char *old);
void		my_cannot_acces(char *path, int a);
int		my_check_str(char c);
int		my_getnbr(char *str);
int		my_strcmp(char *s1, char *s2);
void		get_cmd_pipes(t_sh *sh);
void		get_redirect(char *str, int value, t_sh *sh);
int		is_redi(char *str);
int		get_rdi_value(char *str);
char		**redi_command(t_sh *sh, int idx);
int		tab_len(char **);
int		alias2(char ***alias, char *str);
char		*alias42(char *argv, char ***tabalias);
char		*replace(char *str, char **tabalias, int *o);
char		*first_alias2(char *str,
			      char **tabalias,
			      char *word,
			      char *rez);
char		*first_alias(char *str, char **tabalias);
int		my_builtin_check(char **av);
int		check_errors(char *input);
int		end_curly(char *str);
char		*get_main_word(char *str, int size);
char		*get_braces(char *str);
char		*expand(char *str, int size, int i);
char		*exp_braces(char *str);
int		ihnibited(char *str, int i);
void		turn_back(t_cmd *cmd);
int		mismatched(char *str, int i, char end);
void		double_quote(t_sh *sh, char quote);
void		reset_signals(void);
void		close_fds(int pipefd[], int pipesave[], t_sh *sh);
void		echo_args(char **args);
void		child_exec(t_sh *sh, int count, int pipefd[], int pipesave[]);
int		is_sep(char *str, int i);
char		*epure_command(char *command, int i, int j);
void		manage_command(char **command);
char		**my_setpwd_pwd(char **envp);
int		my_tilde_minus(char **argv, char ***envp);
int		my_cd2(char ***envp);
int		my_cd3(char **argv, char ***envp, t_sh *sh);
int		my_cd(char **argv, char ***envp, t_sh *sh);
char		**my_malloc_envp(char **envp, char **my_envp);
char		*build_str(char **env);
char		**my_setenv(char **envp, char **env);
int		int_len(int *);
int		*remove_int(int *, int idx);
char		**remove_str(char **, int idx);
void		disp_signal(int status);
int		my_strncmp(char *s1, char *s2, int n);
char		**my_malloc_unsetenv(char **envp, char **my_envp, int n);
char		**my_unsetenv(char **envp, char *env);
void		call_me_daddy(t_sh *sh, pid_t res, int i, int idx);
char		*line_editor(char *);
void		read_while(struct termios *, char *, char *, char **);
void		mid_while(t_flag *, int *, char *, struct winsize **);
void		treat_chain(char *, int *, t_flag *, char **);
void		init_flags(t_flag *);
void		canon_off(struct termios *);
void		re_canon(struct termios *);
void		go_left(void);
void		go_right(void);
void		my_replace_cursor(int, char *, int, t_flag *);
void		reaff_line(t_flag *, char *, int *, int);
char		*get_next_word(char *, int);
void		reaff_multilines_down(char *, int *, t_flag *, int);
void		reaff_multilines_up(char *, int *, t_flag *, int);
void		reaff_autocomplete(char *, int, t_flag *, int);
void		reaff_cmd(char *, int *, t_flag *, int);
struct winsize	*check_line(t_flag *, char *);
void		update_total_lines(t_flag *, char *, int);
void		fill_flags_line(t_flag *, char *, int);
int		init_edition(t_flag *, char **, int *, char *);
int		init_edition2(struct termios *, char **, char **);
void		my_autocomplete(char **, int, t_flag *);
t_list		*fill_linked_list(t_list *, char *);
int		find_file(char *, char *, int, char **);
int		find_begin(char *, int);
char		*get_sub(char *, int);
int		init_autocomplete(t_list **, int *);
char		*get_sub_folder(char *);
char		*remove_folder_sub(char *, char *);
char		*get_folder(char *, int);
char		*cut_chain(char *, int);
int		my_rewrite2(char **, char *, int, int);
void		my_rewrite(char **, char *, int *);
void		my_erase(int *, char **);
void		my_move(char *, int *, t_flag *);
void		add_letter(char *, char **, int *);
void		get_high_left(int);
void		get_high(int);
void		get_down(void);
void		show_completion(char *, char *);
void		show_files(char *, char *);
int		my_ask(void);
char		*remove_prompt(char *, char *);
char		*my_strcpy(char *);
int		to_history(char *);
void		check_process(int signum);
void		globber(char ***final, char *to_glob);
int		malloc_my(char **input, char *str, char **local, char **envp);
int		search_long(char **local, char *rez, int *longueur);
int		aff_history(void);
void		init_pipes(int[], int[], int *);
void		*xmalloc(size_t);
void		parse_alias(t_sh *sh, int i);
int		exec_arg(t_sh *sh, int i);
void		init_pipes(int pipefd[2], int pipesave[2], pid_t *pg);
void		close_pipes(int pipefd[2], int pipesave[2], t_sh *sh);
int		new_process(t_sh *sh, pid_t *pg, int idx, t_pipe *pipes);
void		builtin_3(char **av, t_sh *sh, int i);
void		builtin_2(char **av, char ***envp, t_sh *sh, int i);

#endif /* !SH_H_ */
