/*
** error.c for 42sh in /home/elkaim_r/Documents/42sh
** 
** Made by elkaim_r
** Login   <elkaim_r@epitech.net>
** 
** Started on  Thu May 15 10:42:43 2014 elkaim_r
** Last update Sun May 25 17:31:25 2014 elkaim_r
*/

#include <stdlib.h>
#include <stdio.h>
#include "sh.h"

int	check_errors(char *input)
{
  char	**args;
  int	i;
  int	error;

  i = 0;
  error = 0;
  args = my_str_to_wordtab(input, ' ');
  if (!args || !args[0])
    return (1);
  while (args[i] != NULL)
    {
      if ((is_sep(args[i], 0) || !my_strcmp(args[i], "|"))
	  && (!args[i + 1]))
	  error = 1;
      ++i;
    }
  if (error)
    {
      fprintf(stderr, "parse error\n");
      return (1);
    }
  return (0);
}
